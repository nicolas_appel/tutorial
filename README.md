COSMOS 101
==========

COSMOS is the ***Comprehensive Open-architecture Solution for Mission Operations Systems***, a software framework with source code for space mission operations, simulation and flight software. Currently COSMOS is in alpha release mode. The following table shows the COSMOS v1.0 release plan.

Phase             | Original Plan  | Updated Plan | Realized 
----------------: | :------------- |:-----------: | :-----:    | 
Alpha release     |  Jan 2015      | Jan 2015     | Jan 2015
Beta release      |  Aug 2015      | -            | -
Release candidate |  Oct 2015      | -            | -
Final release     |  Dec 2015      | -            | -

We are in the process of updating the COSMOS website but it is still a good place to get started:
[http://www.cosmos-project.org/](http://www.cosmos-project.org/)

This tutorial is currently geared towards COSMOS developers. Later it will be expanded for regular users. We will guide you as if you have never used Git, SourceTree and other cool things we use. If you feel that some information is not clear feel free to speak up and let us know (or add your contributions directly to this repo).
 
You will learn how to setup the COSMOS Git repositories hosted in Bitbucket using SourceTree. SourceTree is one of the best Git clients available so we recommend to use it but you may also use Git from the command line, per your preference. 

If you have landed in this tutorial it's because you probably have been given access to the source code. The credentials that were given to you must be kept private for your only personal use. If you'd like to have more users use the COSMOS repositories have them contact us directly.


## Start here
**1) setup a git client **

All the cosmos repositories are git. If you are using Linux you should have a git client already installed. Check this by opening a terminal an type the git command.

    $ git --version 

you should then see the version of your git client. If not you have to install one.

For Mac or Windows there is a great git client tool developed by Atlassian: SourceTree. We recommend you use it: 
  
* [Download SourceTree](https://www.sourcetreeapp.com/) 

This is Atlassian's Git client. Atlassian is the mother company that develops BitBucket.

**2) Setup the cosmos-source folder **

Create the **cosmos-source** folder anywhere in your computer (any folder that you have read and write access to). 
This will be where all the cosmos source code will reside. 
It is really up to the user but by default we recommend that the **cosmos-source** folder reside in:

* Linux: **/home/<user>/cosmos-source**
* Windows: **C:\Users\<user>\cosmos-source**
* MacOS: **/Users/<user>/cosmos-source**

Here is the structure for the cosmos-source folder (note that the installation folder may have a different structure as needed)

```
cosmos-source
|_ core
|_ tools
|   |_ MOST
|   |_ CEO
|   |_ ...
|_ documentation
|_ projects (your cosmos dependent projects go here by default)
|   |_ my_project_1
|   |   |_ programs
|   |   |_ libraries
|   |   |_ build
|   |_ my_project_2
|   |   |_ programs
|   |   |_ libraries
|   |   |_ build
|_ nodes
|   |_ cubesat1
|   |_ cubesat2
|   |_ .
|   |_ .
|_ resources
|_ thirdparty
```

this folder is dedicated to developers, it should be the only folder to contain source code. The above structure can be changed but the developer must know what to do to setup the proper compilation paths and variables.

**3) Clone the COSMOS repositories into cosmos-source ** 

The repos should be placed inside your cosmos-source folder. Create one folder for each repo that you will be downloading. You must replace <your-bitbucket-username> in the following urls by your own BitBucket account username, for this to work you must have been been given specific permissions to a COSMOS repository.
Typically you will have 

- **cosmos-source/core** for https://<your-bitbucket-username>@bitbucket.org/cosmos/core.git
- **cosmos-source/nodes** for https://<your-bitbucket-username>@bitbucket.org/cosmos/nodes.git
- **cosmos-source/resources** for https://<your-bitbucket-username>@bitbucket.org/cosmos/resources.git
- etc ... 

The easiest way to get the repo links is to open your browser on [https://bitbucket.org/](https://bitbucket.org/), click to the repository you want to download and on the right hand side of the repo page you will see the repo link (using the HTTPS link is a bit simpler than SSH because then you don't have to setup private keys). To make it even easier you can click to the button with the down arrow on the left side of link url and this will open SourceTree automatically with the right link. One other way is by opening SourceTree and click '+ New Repository' and add your links there.

If you are not using SourceTree for any reason (but if you can please do use SourceTree) you can get the repos through the command line

```
$ cd cosmos-source
$ git clone https://<your-bitbucket-username>@bitbucket.org/cosmos/core.git
$ git clone https://<your-bitbucket-username>@bitbucket.org/cosmos/nodes.git
$ git clone https://<your-bitbucket-username>@bitbucket.org/cosmos/resources.git
```

For a more extended list of available COSMOS repositories go to

- [https://bitbucket.org/cosmos/tutorial/src/master/repolist.md](https://bitbucket.org/cosmos/tutorial/src/master/repolist.md)

**4) Setup the COSMOS folder **

Now let's select a COSMOS installation folder. This IS the "COSMOS" folder. 
This will be where operational files, such as executable binaries, compiled libraries, etc. end up. 
We highly recommend that you setup the COSMOS folder in the following suggested location depending on your system:

* Linux: **/usr/local/cosmos**
* Windows: **C:\cosmos**
* MacOS: **/Applications/cosmos**

If you do not have write permission to the folders (eg: /usr/local/), or you choose
your own location, then you will need to set the "COSMOS" environment variable to your non-custom path.

The following is a typical structure you will find inside the "COSMOS" folder.
```
COSMOS
|_ bin
|_ lib
|_ include
|_ nodes
|_ resources
|_ documentation
|_ tools
    |_ MOST
    |_ CEO
    |_ ...
```

**5) Populate the COSMOS folder **

Copy the contents from cosmos-source/resources to cosmos/resources. 
Copy any nodes you wish to work with from cosmos-source/nodes to cosmos/nodes.

**6) install Qt ** 

We recommend using Qt5.4 because it is compatible with most of the COSMOS tools. Go to this link:
* [http://www.qt.io/download-open-source/](http://www.qt.io/download-open-source/)

For Windows: select Mingw as your main compiler, you may also choose to select MSVC as an alternative 
compiler but there may be some compatibility issues with COSMOS since it is not fully supported at this time.

**7) Compile **

To start compiling COSMOS open the cosmos-source/core folder and follow the instruction in the [README.md](https://bitbucket.org/cosmos/core)


Resources
---------
To learn more about Git, SourceTree and Bitbucket follow this link 
[bitbucket tutorial](bitbucket.md)


FAQ's
-----

**Q**: I've installed sourcetree and logged on to bitbucket.  I can't find the cosmos repositories. Any suggestions?

After putting your credentials in SourceTree it will not show the COSMOS repositories automatically. You have to manually add them as described in step 3). This is a bit confusing because when you login into the BitBucket website it will actually show you the COSMOS repositories, given that you have been granted access to the COSMOS repos.