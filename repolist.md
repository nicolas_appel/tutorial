# COSMOS / core (*)#

Main repository for COSMOS core

* [https://bitbucket.org/cosmos/core.git](https://bitbucket.org/cosmos/core.git)

# COSMOS / resources (*)#
The resources repo contains a lot of usefull (and required) resources to run the agent and tools

* [https://bitbucket.org/cosmos/resources.git](https://bitbucket.org/cosmos/resources.git)

# COSMOS / nodes #
You just need the nodes repo to run the examples

* [https://bitbucket.org/cosmos/nodes.git](https://bitbucket.org/cosmos/nodes.git)

# COSMOS / thirdparty  #
The thirdparty repository is usefull but not required for a basic COSMOS version

* [https://bitbucket.org/cosmos/thirdparty.git](https://bitbucket.org/cosmos/thirdparty.git)

# COSMOS / Tools #

The COSMOS tools are the graphic user interface (GUI) tools to 
enable the user to operate. 

Currently (March 2015) the tools are being updated to be compatible 
with the new Qt 5.4 version. Most tools should compile well using 
Qt5.3. On Windows MinGW is required but an effort is being made to 
make the code compatible with MSVC 2013.

# COSMOS / Tools / Mission Operations Support Tool (MOST) #

* [https://bitbucket.org/cosmos/tool-mission-operations-support-tool-most](https://bitbucket.org/cosmos/tool-mission-operations-support-tool-most)

# COSMOS / Tools / libraries #

* [https://bitbucket.org/cosmos/tool-libraries](https://bitbucket.org/cosmos/tool-libraries)

# COSMOS / Tools / Agent Plot Data #

* [https://bitbucket.org/cosmos/tool-agent-plot-data](https://bitbucket.org/cosmos/tool-agent-plot-data)

# COSMOS / Tools / Command Assembler #

* [https://bitbucket.org/cosmos/tool-command-assembler](https://bitbucket.org/cosmos/tool-command-assembler)

# COSMOS / Tools / COSMOS Executive Operator (CEO) #

* [https://bitbucket.org/cosmos/tool-cosmos-executive-operator-ceo](https://bitbucket.org/cosmos/tool-cosmos-executive-operator-ceo)

# COSMOS / Tools / Data Management Tool #

* [https://bitbucket.org/cosmos/tool-data-management-tool-dmt](https://bitbucket.org/cosmos/tool-data-management-tool-dmt)


